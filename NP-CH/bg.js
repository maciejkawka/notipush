function generateFirstPartID() {
    var id = new Date().getFullYear().toString().substr(2, 2);
    var miesiac = new Date().getMonth();

    switch (miesiac) {
        case 0:
            id = id + 'AX';
            break;
        case 1:
            id = id + 'BX';
            break;
        case 2:
            id = id + 'CX';
            break;
        case 3:
            id = id + 'DX';
            break;
        case 4:
            id = id + 'EX';
            break;
        case 5:
            id = id + 'FX';
            break;
        case 6:
            id = id + 'GX';
            break;
        case 7:
            id = id + 'HX';
            break;
        case 8:
            id = id + 'IX';
            break;
        case 9:
            id = id + 'JX';
            break;
        case 10:
            id = id + 'KX';
            break;
        case 11:
            id = id + 'LX';
            break;
        default:
            id = id + "MX";
            console.log('error during generation');
    }

    var dzien = new Date().getDate();

    if (dzien < 10)
        id = id + '0' + dzien;
    else
        id = id + dzien;

    return id;
}

function generateID() {

    var id = generateFirstPartID();
    var random = Math.floor(Math.random() * 1000) + 1;
    id += random;

    return id;
}


/* --- f: m_komunikat --- */
function m_komunikat(komunikat) {
    var element_okno = document.getElementById("div_komunikat");
    /* --- otwórz okno, jeśli jeszcze nie istnieje --- */
    if (!element_okno) {
        /* --- div komunikatu --- */
        m_komunikat_tresc_okno = document.createElement("DIV");
        m_komunikat_tresc_okno.setAttribute("id", "div_komunikat");
        m_komunikat_tresc_okno.style.position = "fixed";
        m_komunikat_tresc_okno.style.zIndex = "99995";
        m_komunikat_tresc_okno.style.top = "10px";
        m_komunikat_tresc_okno.style.left = "calc(50% - 160px)";
        m_komunikat_tresc_okno.style.width = "300px";
        m_komunikat_tresc_okno.style.backgroundColor = "#ECFADE";
        m_komunikat_tresc_okno.style.border = "1px solid #CFDDC1";
        m_komunikat_tresc_okno.style.textAlign = "right";
        m_komunikat_tresc_okno.style.padding = "10px";
        m_komunikat_tresc_okno.style.fontSize = "14px";
        m_komunikat_tresc_okno.style.fontWeight = "normal";
        m_komunikat_tresc_okno.style.fontFamily = "Tahoma,Arial,Helvetica,sans-serif";
        /* + */
        document.body.appendChild(m_komunikat_tresc_okno);
        /* --- /div komunikatu --- */

        /* --- div zamykacza --- */
        m_komunikat_tresc_zamykacz = document.createElement("DIV");
        m_komunikat_tresc_zamykacz.setAttribute("id", "div_zamykacz");
        m_komunikat_tresc_zamykacz.style.width = "20px";
        m_komunikat_tresc_zamykacz.style.height = "20px";
        m_komunikat_tresc_zamykacz.style.marginLeft = "auto";
        m_komunikat_tresc_zamykacz.style.marginBottom = "-20px";
        m_komunikat_tresc_zamykacz.style.textAlign = "center";
        m_komunikat_tresc_zamykacz.style.color = "red";
        m_komunikat_tresc_zamykacz.style.fontWeight = "bold";
        m_komunikat_tresc_zamykacz.style.cursor = "default";
        m_komunikat_tresc_zamykacz.innerHTML = "&#10799;";
        /* + */
        m_komunikat_tresc_okno.appendChild(m_komunikat_tresc_zamykacz);
        /* --- /div zamykacza --- */

        /* --- div treści --- */
        m_komunikat_tresc = document.createElement("DIV");
        m_komunikat_tresc.setAttribute("id", "div_tresc");
        m_komunikat_tresc.style.marginLeft = "20px";
        m_komunikat_tresc.style.marginRight = "20px";
        m_komunikat_tresc.style.textAlign = "center";
        m_komunikat_tresc.innerHTML = "";
        /* + */
        m_komunikat_tresc_okno.appendChild(m_komunikat_tresc);
        /* --- /div treści --- */

        /* --- obsługa zamykania --- */
        document.addEventListener('click', function(e) {
            if (e.srcElement.id == "div_zamykacz") m_komunikat();
        });
        /* --- /obsługa zamykania --- */
    }
    /* --- /otwórz okno, jeśli jeszcze nie istnieje --- */

    /* --- treść komunikatu --- */
    if (komunikat != "") {
        var element_komunikat = document.getElementById("div_tresc");
        element_komunikat.innerHTML = komunikat;
    }
    /* --- /treść komunikatu --- */

    /* --- kasuj okno --- */
    if (komunikat == "") {
        element_okno.parentNode.removeChild(element_okno);
        delete m_komunikat_tresc_okno;
    }
    /* --- /kasuj okno --- */
}
/* --- /f: m_komunikat --- */

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyDtg_xBIouML__A3g0TlJanC8Nltb2Gfq8",
    authDomain: "notipush-7818d.firebaseapp.com",
    databaseURL: "https://notipush-7818d.firebaseio.com",
    projectId: "notipush-7818d",
    storageBucket: "notipush-7818d.appspot.com",
    messagingSenderId: "383925110727",
    appId: "1:383925110727:web:665b2f8de72794cbe9bbe2",
    measurementId: "G-MN6RC3N16V"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

firebase.analytics();

function doInCurrentTab(tabCallback) {
    chrome.tabs.query({ currentWindow: true, active: true },
        function(tabArray) { tabCallback(tabArray[0]); }
    );
}

function removeData(danychild) {


}


function wTle(idToListen, myId) {

    var db2 = firebase.database();
    var ch2 = db2.ref().child('DB_CH');

    var q2 = ch2.child(idToListen + '_' + myId).on('value', function(datasnapshot) {
        if (datasnapshot.numChildren() > 1) {
            var apka = datasnapshot.child('appname').val();
            var text = datasnapshot.child('text').val();
            var time = datasnapshot.child('time').val();
            var title = datasnapshot.child('title').val();
            //alert(apka + '_' + text + '_' + time + '_' + title);

            var notification = {
                type: 'basic',
                iconUrl: 'image.png',
                title: apka + " : " + time,
                message: title + '\n' + text
            };
            chrome.notifications.create('limitNotif', notification);

        }

    });

}

chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {



    if (msg.command == "NPnewIdG") {
        var newId = generateID();
        var firsPartId = generateFirstPartID();
        var database = firebase.database();
        var child = database.ref().child('DB_ID_EX');

        var sorted = child.orderByKey().startAt(firsPartId).once("value") //tu jeszcze sprawdzić na większej ilośći danych czy poprawnie wycina tylko z danego dnia
            .then(function(snapshot) {
                if (snapshot.numChildren() == 0) {
                    var query = child.child(newId).set('off');
                    chrome.storage.sync.set({ myID: newId });
                    sendResponse({ response: "good" });
                } else {
                    const tab = new Array(snapshot.numChildren());
                    var i = 0;
                    snapshot.forEach(function(childSnapshot) {
                        tab[i] = childSnapshot.key;
                        i++;
                    });
                    var max = 0;
                    var petla = true;
                    while (petla) {
                        if (max == 999) {
                            newId = newId + (Math.floor(Math.random() * 1000) + 1);
                        }
                        for (var a = 0; a < tab.length; a++) {
                            if (tab[a] == newId) {
                                newId = generateID();
                                break;
                            }

                            if (a == (tab.length - 1)) {
                                petla = false;
                            }
                        }
                        max++;
                    }
                    var query = child.child(newId).set('off');
                    chrome.storage.sync.set({ myID: newId });
                    sendResponse(newId);
                }
            });
    }
    if (msg.command == "idToListen") {

        var myId;
        chrome.storage.sync.get(['myID'], function(myId) {

            var idToListen = msg.data.data;
            var database = firebase.database();
            var child = database.ref().child('ID_PHONES');
            var query = child.child(idToListen).set(myId);

        });
    }

    if (msg.command == "listen") {

        var myId;
        chrome.storage.sync.get(['myID'], function(myId) {

            var d1 = firebase.database();
            var ch1 = d1.ref().child('ID_PHONES');
            var q1 = ch1.child(msg.data.val).set(myId.myID)

            var db = firebase.database();
            var ch = db.ref().child('DB_ID_EX');
            var q = ch.child(myId.myID).on('value', function(datasnapshot) {
                if (datasnapshot.numChildren() >= 3) {
                    var apka = datasnapshot.child('appname').val();
                    var text = datasnapshot.child('text').val();
                    var time = datasnapshot.child('time').val();
                    var title = datasnapshot.child('title').val();

                    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
                        if (tabs[0].id != null)
                            chrome.tabs.sendMessage(tabs[0].id, { msg: "msg", apka: apka, text: text, time: time, title: title }, function(response) {

                            });
                    });

                }
            });


        });

    }

});