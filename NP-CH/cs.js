function showPopup(data = "", apka = "", tytul = "", tresc = "") {
    var popupNotiElement = document.getElementById("NPdivforAll");
    /* --- otwórz okno, jeśli jeszcze nie istnieje --- */
    if (!popupNotiElement) {
        /* --- div komunikatu --- */
        NotiPushforAll = document.createElement("DIV");
        NotiPushforAll.setAttribute("id", "NPdivforAll");
        NotiPushforAll.style.position = "fixed";
        NotiPushforAll.style.zIndex = "99995";
        NotiPushforAll.style.top = "10px";
        NotiPushforAll.style.left = "calc(95% - 300px)";
        NotiPushforAll.style.width = "300px";
        NotiPushforAll.style.backgroundColor = "#404040";
        NotiPushforAll.style.border = "2px solid #0fab15";
        NotiPushforAll.style.textAlign = "right";
        NotiPushforAll.style.padding = "10px";
        NotiPushforAll.style.fontSize = "14px";
        NotiPushforAll.style.fontWeight = "normal";
        NotiPushforAll.style.fontFamily = "Tahoma,Arial,Helvetica,sans-serif";
        /* + */
        document.body.appendChild(NotiPushforAll);
        /* --- /div komunikatu --- */

        /* --- div zamykacza --- */
        NotiPushforClose = document.createElement("DIV");
        NotiPushforClose.setAttribute("id", "NPdivforClose");
        NotiPushforClose.style.width = "20px";
        NotiPushforClose.style.height = "20px";
        NotiPushforClose.style.marginLeft = "auto";
        NotiPushforClose.style.marginBottom = "-20px";
        NotiPushforClose.style.textAlign = "center";
        NotiPushforClose.style.color = "red";
        NotiPushforClose.style.fontWeight = "bold";
        NotiPushforClose.style.cursor = "default";
        NotiPushforClose.innerHTML = "&#10799;";
        NotiPushforClose.addEventListener('click', function(e) {
            NotiPushforAll.parentNode.removeChild(NotiPushforAll);
        });
        /* + */
        NotiPushforAll.appendChild(NotiPushforClose);
        /* --- /div zamykacza --- */

        NotiPushforLogo = document.createElement("DIV");
        NotiPushforLogo.setAttribute("id", "NPdivforLogo");
        NotiPushforLogo.style.marginLeft = "20px";
        NotiPushforLogo.style.marginBottom = "5px";
        NotiPushforLogo.style.color = "white";
        NotiPushforLogo.style.marginRight = "20px";
        NotiPushforLogo.style.textAlign = "center";
        NotiPushforLogo.style.fontSize = "18";
        NotiPushforLogo.innerHTML = "Noti Push";
        /* + */
        NotiPushforAll.appendChild(NotiPushforLogo);

        /* --- div treści --- */
        NotiPushforData = document.createElement("DIV");
        NotiPushforData.setAttribute("id", "NPdivforDate");
        NotiPushforData.style.marginLeft = "20px";
        NotiPushforData.style.marginBottom = "5px";
        NotiPushforData.style.color = "white";
        NotiPushforData.style.border = "2px solid #0fab15";
        NotiPushforData.style.marginRight = "20px";
        NotiPushforData.style.textAlign = "center";
        NotiPushforData.innerHTML = "";
        /* + */
        NotiPushforAll.appendChild(NotiPushforData);
        /* --- /div treści --- */

        /* --- div treści --- */
        NotiPushforAppName = document.createElement("DIV");
        NotiPushforAppName.setAttribute("id", "NPdivforAppName");
        NotiPushforAppName.style.border = "2px solid #0fab15";
        NotiPushforAppName.style.marginLeft = "20px";
        NotiPushforAppName.style.color = "white";
        NotiPushforAppName.style.marginBottom = "5px";
        NotiPushforAppName.style.marginRight = "20px";
        NotiPushforAppName.style.textAlign = "center";
        NotiPushforAppName.innerHTML = "";
        /* + */
        NotiPushforAll.appendChild(NotiPushforAppName);
        /* --- /div treści --- */

        /* --- div treści --- */
        NotiPushforTitle = document.createElement("DIV");
        NotiPushforTitle.setAttribute("id", "NPdivforTitle");
        NotiPushforTitle.style.marginLeft = "20px";
        NotiPushforTitle.style.marginBottom = "5px";
        NotiPushforTitle.style.color = "white";
        NotiPushforTitle.style.border = "2px solid #0fab15";
        NotiPushforTitle.style.marginRight = "20px";
        NotiPushforTitle.style.textAlign = "center";
        NotiPushforTitle.innerHTML = "";
        /* + */
        NotiPushforAll.appendChild(NotiPushforTitle);
        /* --- /div treści --- */
        /* --- div treści --- */
        NotiPushforConntent = document.createElement("DIV");
        NotiPushforConntent.setAttribute("id", "NPdivforConntent");
        NotiPushforConntent.style.border = "2px solid #0fab15";
        NotiPushforConntent.style.marginLeft = "20px";
        NotiPushforConntent.style.marginBottom = "5px";
        NotiPushforConntent.style.color = "white";
        NotiPushforConntent.style.marginRight = "20px";
        NotiPushforConntent.style.textAlign = "center";
        NotiPushforConntent.innerHTML = "";
        /* + */
        NotiPushforAll.appendChild(NotiPushforConntent);
        /* --- /div treści --- */


    }


    /* --- treść komunikatu --- */
    if (data != "") {
        var element_komunikat = document.getElementById("NPdivforDate");
        element_komunikat.innerHTML = data;
    }

    if (apka != "") {
        var element_komunikat = document.getElementById("NPdivforAppName");
        element_komunikat.innerHTML = apka;
    }

    if (tytul != "") {
        var element_komunikat = document.getElementById("NPdivforTitle");
        element_komunikat.innerHTML = tytul;
    }

    if (tresc != "") {
        var element_komunikat = document.getElementById("NPdivforConntent");
        element_komunikat.innerHTML = tresc;
    }

}



chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {

    if (msg.msg == "msg") {
        var value3;
        chrome.storage.sync.get(['lastNoti'], function(value3) {
            if (value3.lastNoti) {
                if (msg.time != value3.lastNoti.postTime && msg.text != value3.lastNoti.message) {

                    showPopup(msg.time, msg.apka, msg.title, msg.text); //ta funkcja z popupem.

                    var noti = {
                        appname: msg.apka,
                        message: msg.text,
                        postTime: msg.time,
                        mesTitle: msg.title
                    }

                    chrome.storage.sync.set({ 'lastNoti': noti }, function() {

                    });


                }

            } else {
                showPopup(msg.time, msg.apka, msg.title, msg.text); //ta funkcja z popupem.

                var noti = {
                    appname: msg.apka,
                    message: msg.text,
                    postTime: msg.time,
                    mesTitle: msg.title
                }

                chrome.storage.sync.set({ 'lastNoti': noti }, function() {

                });

            }



        });



    }

    sendResponse("pokazane");

});