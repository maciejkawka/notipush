function newf() {
    var value;
    var value2;
    var value3;
    var value4;
    var status;

    chrome.storage.sync.get(['myID'], function(value) {
        if (value.myID) {
            $('#forMyid').text("My ID: " + value.myID);
        } else {
            chrome.runtime.sendMessage({ command: "NPnewIdG" }, (response) => {
                $('#forMyid').text("My ID: " + response);
            });
        }
    });

    chrome.storage.sync.get(['idToListen'], function(value2) {
        if (value2.idToListen) {
            $('#listenedID').val(value2.idToListen);
        } else {
            // alert("nie ma");
        }
    });

    chrome.storage.sync.get(['lastNoti'], function(value3) {
        if (value3.lastNoti) {
            $('#NPlastdate').text(value3.lastNoti.postTime);
            $('#NPlastapp').text(value3.lastNoti.appname);
            $('#NPlasttitile').text(value3.lastNoti.mesTitle);
            $('#NPlastmessage').text(value3.lastNoti.message);
            $('#NPInfo').text("Last Notyfication:");
        } else {
            $('#forConnected').text(' ');
            // alert("nie ma");
        }
    });

    chrome.storage.sync.get(['status'], function(value4) {
        if (value4.status) {
            status = value4.status;
            if (status == true) {
                $('#myButton').attr({ "value": "Stop ?" });
                $('#myButton').css("background-color", "#CC0000");
            } else {
                $('#myButton').attr({ "value": "listen ?" });
                $('#myButton').css("background-color", "#0fab15");
            }
        } else {
            status = false;
            chrome.storage.sync.set({ status: status });
        }
    });

};

newf();


$(document).ready(function() {
    $("#myButton").click(function() {
        var str = $("#listenedID").val();
        if (str) {
            var stat;
            chrome.storage.sync.get(['status'], function(stat) {
                if (stat) {
                    if (stat.status == true) {
                        $('#myButton').attr({ "value": "listen ?" });
                        $('#myButton').css("background-color", "#0fab15");
                        chrome.storage.sync.set({ status: false });
                        chrome.storage.sync.set({ idToListen: str });
                        //Tutaj na odłączenie słuchacza

                    } else {
                        $('#myButton').attr({ "value": "Stop ?" });
                        $('#myButton').css("background-color", "#CC0000");
                        chrome.storage.sync.set({ status: true });
                        chrome.storage.sync.set({ idToListen: str });
                        chrome.runtime.sendMessage({ command: "listen", data: { val: str } }); // podpięcie słuchacza
                    }
                } else {
                    stat = false;
                    chrome.storage.sync.set({ status: stat });
                }
            });

        } else {
            alert("Put ID your Device");
        }
    });
});