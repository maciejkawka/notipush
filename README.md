## ABOUT 

NotiPush is an Android mobile application that works with the chorm browser extension (in the NP-CH folder) and sends notifications to the PC screen
### You can: 


- create an account / log in
- check installed applications
- choose from which applications to collect notifications 
- save the notification in memory
- view saved notifications
- present a graphical summary of the number of notifications
- connect to chrome browser extension
- send in real-time notification to the currently active browser tab


### Technologies:

- Java (8+)
- Android 
- MVC architecture
- Firebase
- JavaScript
- Chrome extension architecture


### Preview:

- Settings: </br>
<img src="sett_layo.jpg" with="210px" height="512px">
</br></br>

- Statistics: </br>
<img src="statistics+layout.jpg" with="210px" height="512px">
</br></br>

- Saved notifications:</br>
<img src="history_layout.jpg" with="210px" height="512px">

- Browser notification: </br>
<img src="popup.png" with="210px" height="512px">

### Authors:

- Maciej Kawka
