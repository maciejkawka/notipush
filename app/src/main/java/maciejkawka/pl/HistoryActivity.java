package maciejkawka.pl;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.gson.Gson;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import maciejkawka.pl.adapters.OnHistoryClick;
import maciejkawka.pl.adapters.HistoryAdapterWithViewHolder;
import maciejkawka.pl.objects.SavedNotification;

public class HistoryActivity extends AppCompatActivity implements OnHistoryClick

{

    private RecyclerView recyclerView;


    private RecyclerView.LayoutManager mLayoutManager;
    private HistoryAdapterWithViewHolder mAdapter;
    private List<SavedNotification> mList;

    @Override
    protected void onCreate(final Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        this.setTitle("History on the device");

        recyclerView = findViewById(R.id.statisticRecyclerView);


        recyclerView.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(HistoryActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

                if(mList == null) {
                    mList = new ArrayList<>();
                }
                if(mList.size() > 0) {
                    mList.clear();
                }

                loadSavedNotyfications();
                if(mList.size()>0){
                    Collections.sort(mList);
                    Collections.reverse(mList);
                }

                mAdapter = new HistoryAdapterWithViewHolder(mList, HistoryActivity.this, this);
                recyclerView.setAdapter(mAdapter);

    }





    private void createDialog(int position) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.statistic_dialog);
        ImageView closeImage = dialog.findViewById(R.id.dialogStatisticClose);
        ImageView notiImage = dialog.findViewById(R.id.sfforImage);
        TextView notiLabel = dialog.findViewById(R.id.sdforLabel);
        TextView notidate = dialog.findViewById(R.id.sdforDate);
        TextView notititle = dialog.findViewById(R.id.sdforTitle);
        TextView notiText = dialog.findViewById(R.id.sfforText);
        SavedNotification notyfikacja = mList.get(position);
        notidate.setText(notyfikacja.getDate().toString());
        notiLabel.setText(notyfikacja.getAppName());
        notititle.setText(notyfikacja.getTitle());
        notiText.setText(notyfikacja.getText());
        PackageManager packageManager = getPackageManager();
        try{
            ApplicationInfo app = packageManager.getApplicationInfo(notyfikacja.getPackageName(),0);
            Drawable icon = app.loadIcon(packageManager);
            notiImage.setBackgroundDrawable(icon);
        } catch (PackageManager.NameNotFoundException e)
        {

            if((e.getStackTrace() != null))
                Log.e("Statistic, Dialog" ,e.getMessage());

            if(e.getMessage() != null)
                Log.e("Statistic, Dialog" ,e.getMessage());
        }

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void loadSavedNotyfications()
    {
        if(mList == null)
            mList = new ArrayList<>();

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.spnotyfications),MODE_PRIVATE);

        Gson gson = new Gson();

        Map<String, ?> all = sharedPreferences.getAll();

        for(Map.Entry<String, ? > noty : all.entrySet())
        {
            String json = (String) noty.getValue();
            SavedNotification notyfication = gson.fromJson(json,SavedNotification.class);

            mList.add(notyfication);
        }
        Log.d("StatisticActivity","Załadowało: "+mList.size());


    }


    @Override
    public void onStatisticItemClick(int position) {
        createDialog(position);
    }
}