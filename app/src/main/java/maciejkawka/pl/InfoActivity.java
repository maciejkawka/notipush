package maciejkawka.pl;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    TextView infoFromDB1;
    TextView infoFromDB2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        infoFromDB1 = findViewById(R.id.fromTheDB1);
        infoFromDB2 = findViewById(R.id.fromTheDB2);

        }
}