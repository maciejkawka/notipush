package maciejkawka.pl;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import maciejkawka.pl.objects.GenerateID;

public class MainActivity extends AppCompatActivity {

    private ActionBarDrawerToggle toogle;
    private ProgressBar progressBar;
    private TextView forId;
    private DrawerLayout drawerLayout;
    private FirebaseAuth fAuth;
    private DatabaseReference mDatabase;

    private TextView mconnectedText;
    private TextView mconnectedID;
    private Button ifDoesntWorkButton;

    private Dialog rePermissionDialog;
    private Dialog whatDoDialog;;


    private ImageView imageQuestion;
    private String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle(R.string.app_name);


        rePermissionDialog = new Dialog(this);
        whatDoDialog = new Dialog(this);

        mconnectedText = findViewById(R.id.mainConnectedWith);
        mconnectedID = findViewById(R.id.mainConnectedID);

        drawerLayout = findViewById(R.id.drawerLayoutLoginMain);
        toogle = new ActionBarDrawerToggle(this,drawerLayout,R.string.Open,R.string.Close);
        drawerLayout.addDrawerListener(toogle);
        toogle.syncState();

        fAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // pojawia te kreski na menu

        imageQuestion = findViewById(R.id.imageQuestion);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if(id==R.id.menu_setings){
                    showToast("Settings");
                    startActivity(new Intent(getApplicationContext(),SetingsActivity.class));

                }
                if(id==R.id.menu_history){
                    showToast("History");
                    startActivity(new Intent(getApplicationContext(), HistoryActivity.class));
                }

                if(id == R.id.menu_statistics){
                    showToast("Statistcs");
                    startActivity(new Intent(getApplicationContext(), StatistcsActivity.class));


                }

                if(id==R.id.menu_info){
                    showToast("Information");
                    startActivity(new Intent(getApplicationContext(),InfoActivity.class));
                }
                return true;
            }
        });


            progressBar = findViewById(R.id.mainProgressBar);
            progressBar.setVisibility(View.INVISIBLE);
            forId = findViewById(R.id.mforId);

        if(fAuth.getCurrentUser() == null)
        {
            startActivity(new Intent(getApplicationContext(),LoginRegister.class));
            finish();
        }
        else
        {
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sfid),MODE_PRIVATE);
            id = sharedPreferences.getString((String)getText(R.string.keyForid),"non");

            if(id.equals("non"))
            {
                showToast(getString(R.string.wait));
                setMyID();
            }
            else
            {
                forId.setText(id);
                mDatabase.child("ID_PHONES").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists())
                        {   }
                        else
                            mDatabase.child("ID_PHONES").child(id).setValue("off");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

            }


            if(!isMyServiceRunning(MainService.class))
            {
                runServis();
                showRePermissionDialog();
            }


            mDatabase.child("ID_PHONES").child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    if(snapshot.exists())
                    {
                        mconnectedText.setVisibility(View.VISIBLE);
                        mconnectedID.setText(snapshot.getValue(String.class));
                        mconnectedID.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    mconnectedText.setVisibility(View.INVISIBLE);
                    mconnectedID.setVisibility(View.INVISIBLE);
                }
                });
        }

        ifDoesntWorkButton = findViewById(R.id.ifdoesntwork);
        ifDoesntWorkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogWhatdoIfnotWorking();
            }
        });

    }

    private void showDialogWhatdoIfnotWorking() {

        whatDoDialog.setContentView(R.layout.if_doesnt_work_dialog);

        Button settingsOpen = whatDoDialog.findViewById(R.id.openSettings);
        Button okcloseDialog = whatDoDialog.findViewById(R.id.closeDialog);
        Button repermission = whatDoDialog.findViewById(R.id.rebutton);

        settingsOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whatDoDialog.dismiss();

                startActivity(new Intent(getApplicationContext(),SetingsActivity.class));
            }
        });

        okcloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whatDoDialog.dismiss();
            }
        });


        repermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whatDoDialog.dismiss();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS), 0);
            }
        });

        whatDoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        whatDoDialog.show();



    }

    private void showRePermissionDialog() {

        rePermissionDialog.setContentView(R.layout.permission_dialog);

        ImageView imageclose = rePermissionDialog.findViewById(R.id.dialogRepermissionClose);
        Button rePermissionButton = rePermissionDialog.findViewById(R.id.dialofRepermissionButton);

        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rePermissionDialog.dismiss();
            }
        });

        rePermissionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rePermissionDialog.dismiss();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS), 0);
            }
        });
        rePermissionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        rePermissionDialog.show();

    }

    private void setMyID() {
        progressBar.setVisibility(View.VISIBLE);
        mDatabase.child("DB_ID_PHONE").child(fAuth.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.child(android.os.Build.MODEL).exists())
                {
                    //jeżeli jest to pobranie wartości
                    id = (String) snapshot.child(android.os.Build.MODEL).getValue();
                    forId.setText(id);
                    progressBar.setVisibility(View.INVISIBLE);
                    saveMynewIDinSheredPreferences(id);
                }
                else {

                    showToast(getString(R.string.noDataforthisDevice) + "\n" + getString(R.string.generate));
                    generateID();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    public void showDialogAlertWhatdoWithId(View v) {

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.wgatDowithId));
        alertDialog.setMessage(getString(R.string.infoWhatDoWithID));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(toogle.onOptionsItemSelected(item)){ // trzy kreski po lewo (akcja otwarcia)
            return true;
        }

        //to jest to menu po prawo.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.infoMenu) {
            startActivity(new Intent(getApplicationContext(),InfoActivity.class));
        }

        if(id == R.id.logoutMenu)
        {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle(getString(R.string.loggout));
            alertDialog.setMessage(getString(R.string.doyologout));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.logo),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            FirebaseAuth.getInstance().signOut();
                            startActivity(new Intent(getApplicationContext(),LoginRegister.class));
                            finish();
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();



        }

        return super.onOptionsItemSelected(item);
    }

    private void generateID() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("ID_PHONES").orderByKey().startAt(GenerateID.yearMonthandDay()).endAt(GenerateID.yearMonthandDay()+"\uf8ff")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String myNEW_ID = "";
                        if(snapshot.exists())
                        {
                            int tmpTab[] = new int[(int) snapshot.getChildrenCount()];
                            int i = 0;

                            for( DataSnapshot data : snapshot.getChildren())
                            {
                                tmpTab[i] = Integer.parseInt(data.getKey().substring(5));
                                i++;
                            }
                            boolean whileLoop = true;
                            int max = 0;

                            while (whileLoop)
                            {
                                myNEW_ID = GenerateID.drawId();
                                if(max > 1000)
                                {
                                    myNEW_ID = myNEW_ID + new Random().nextInt(1000);
                                }
                                int restNewId = Integer.parseInt(myNEW_ID.substring(5));

                                for(int a = 0; a<tmpTab.length ; a++)
                                {
                                    if(restNewId == tmpTab[a])
                                    { break; }

                                    if(a == (tmpTab.length-1))
                                    { whileLoop = false; }
                                }
                            }
                        }
                        else
                        {
                            myNEW_ID = GenerateID.drawId();
                        }


                        mDatabase = FirebaseDatabase.getInstance().getReference();
                        mDatabase.child("ID_PHONES").child(myNEW_ID).setValue("off");
                        mDatabase.child("DB_ID_PHONE").child(fAuth.getUid()).child(android.os.Build.MODEL).setValue(myNEW_ID);
                        forId.setText(myNEW_ID);
                        saveMynewIDinSheredPreferences(myNEW_ID);
                        id = myNEW_ID;

                    }



                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

    }

    private void saveMynewIDinSheredPreferences(String myNEW_id) {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sfid),MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.keyForid),myNEW_id);
        editor.commit();
    }

    private void runServis() {

        Intent serviceIntent = new Intent(this, MainService.class);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startForegroundService(serviceIntent);
        else
            startService(serviceIntent);
    }

//    private void destroyMyService(){
//        Intent serviceIntent = new Intent(this, MainService.class);
//        stopService(serviceIntent);
//    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }



}
