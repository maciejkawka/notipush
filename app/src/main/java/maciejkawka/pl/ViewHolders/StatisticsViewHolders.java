package maciejkawka.pl.ViewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import maciejkawka.pl.R;


public class StatisticsViewHolders extends RecyclerView.ViewHolder {

    public ImageView forImage;
    public TextView forAppName;
    public TextView forQuantity;
    public TextView forPercent;
    public ProgressBar forProgres;

    public StatisticsViewHolders(@NonNull View itemView) {
        super(itemView);

        forImage = itemView.findViewById(R.id.imageforApp);
        forAppName = itemView.findViewById(R.id.apkaName) ;
        forQuantity= itemView.findViewById(R.id.iloscApka) ;
        forPercent = itemView.findViewById(R.id.naProcenty) ;
        forProgres = itemView.findViewById(R.id.progressBar) ;
    }
}
