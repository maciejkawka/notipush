package maciejkawka.pl.ViewHolders;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import maciejkawka.pl.R;
import maciejkawka.pl.adapters.OnHistoryClick;

public class HistoryViewHolder extends RecyclerView.ViewHolder {

    public ImageView mImageView;
    public TextView mDate;
    public TextView mTitle;
    public TextView mText;
    public TextView mAppName;


    public HistoryViewHolder(@NonNull View itemView, final OnHistoryClick mListener) {
        super(itemView);
        mImageView = itemView.findViewById(R.id.imageStatistic);
        mDate = itemView.findViewById(R.id.dateStatistic);
        mTitle = itemView.findViewById(R.id.titleStatistic);
        mText = itemView.findViewById(R.id.textStatistic);
        mAppName = itemView.findViewById(R.id.appNameStatistic);


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    int position = getAdapterPosition();
                    Log.i("ViewHolder","click "+position);
                    if(position != RecyclerView.NO_POSITION){
                        mListener.onStatisticItemClick(position);
                    }

                }
            }
        });
    }


}
