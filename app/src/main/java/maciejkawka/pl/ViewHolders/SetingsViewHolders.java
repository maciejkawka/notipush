package maciejkawka.pl.ViewHolders;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import maciejkawka.pl.R;
import maciejkawka.pl.adapters.OnAppSetingsClick;

public  class SetingsViewHolders extends RecyclerView.ViewHolder  {

    public TextView mTitle;
    public TextView mVerion;
    public ImageView imageView;
    public Switch mSwitch;

    public SetingsViewHolders(View itemView, final OnAppSetingsClick mListener) {
        super(itemView);
        this.mTitle = itemView.findViewById(R.id.appTitle);
        this.mVerion = itemView.findViewById(R.id.versionId);
        this.imageView = itemView.findViewById(R.id.iconImage);
        this.mSwitch = itemView.findViewById(R.id.switSelectedApp);

        mSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null) {
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        boolean status = mSwitch.isChecked();
                        mListener.onItemClick(position, status);
                    }

                }
            }
        });
    }
}
