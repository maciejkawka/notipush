package maciejkawka.pl;

import android.content.Intent;

import android.os.Bundle;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginRegister extends AppCompatActivity {


    private TextView mEmali;
    private TextView mPassword;
    private ProgressBar progressBar;

    FirebaseAuth fAith;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setTitle("login or register");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_register_layout);
        mEmali = findViewById(R.id.mLoginText_LoginRegisterLayout);
        mPassword = findViewById(R.id.mPasswordText_LoginRegisterLayout);
        showToast( getString(R.string.pleaseLogReg));
        progressBar = findViewById(R.id.progressBar2);
        fAith = FirebaseAuth.getInstance();

    }

    public void loginAction_LoginRegisterActivity(View view) {
        if(correctData(mEmali,mPassword))
        {
            String mail = mEmali.getText().toString().trim();
            String pass = mPassword.getText().toString().trim();
            progressBar.setVisibility(View.VISIBLE);
            fAith.signInWithEmailAndPassword(mail,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful())
                    {
                        showToast(getString(R.string.logSucces));
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                    else
                    {
                        showToast(getString(R.string.LoginErr));
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }

    public void registerAction_LoginRegisterActivity(View view) {

        Intent inten = new Intent(this, Registration.class);
        startActivity(inten);;
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

     private boolean correctData(TextView login, TextView pass1 )
     {

        if(login.getText().toString().equals(""))
        {
            login.setError(getString(R.string.required));
            return false;
        }
        if(pass1.getText().toString().equals("")){
            pass1.setError(getString(R.string.required));
            return false;
        }


        if(pass1.getText().toString().length()<=7){
            pass1.setError(getString(R.string.moreLength));
            return false;
        }

        return true;

     }



}