package maciejkawka.pl;

import android.annotation.SuppressLint;
import android.app.admin.DeviceAdminInfo;
import android.bluetooth.BluetoothClass;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import maciejkawka.pl.objects.GenerateID;

public class Registration extends AppCompatActivity {

    private TextView mlogin;
    private TextView mpassword1;
    private TextView mpassword2;
    private ProgressBar progressBar;
    private Button reg;
    private FirebaseAuth fAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        this.setTitle("registration...");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mlogin = findViewById(R.id.emailActivityRegistration);
        mpassword1 = findViewById(R.id.password1ActivityRegistration);
        mpassword2 = findViewById(R.id.password2ActivityRegistration);
        progressBar = findViewById(R.id.progressBar);
        reg = findViewById(R.id.registerActivityRegistration);

        fAuth = FirebaseAuth.getInstance();

        if(fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(correctData(mlogin,mpassword1,mpassword2))
                {
                    String email = mlogin.getText().toString().trim();
                    String pass = mpassword2.getText().toString().trim();
                    progressBar.setVisibility(View.VISIBLE);
                    fAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful())
                            {
                                Toast.makeText(Registration.this, getText(R.string.newusersucces), Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                            }
                            else
                            {
                                progressBar.setVisibility(View.INVISIBLE);
                                Toast.makeText(Registration.this, getText(R.string.NOTnewusersucces)+"/n"+task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });


    }





    private boolean correctData(TextView login, TextView pass1, TextView pass2 ){

        if(login.getText().toString().equals("")){
            login.setError(getString(R.string.required));
            return false;
        }
        if(pass1.getText().toString().equals("")){
            pass1.setError(getString(R.string.required));
            return false;
        }
        if(pass2.getText().toString().equals("")){
            pass2.setError(getString(R.string.required));
            return false;
        }

        if(!pass1.getText().toString().trim().equals(pass2.getText().toString().trim()))
        {
            pass2.setError(getString(R.string.passAllert));
            return false;
        }
        else{
            if(pass2.getText().toString().length()<=7){
                pass2.setError(getString(R.string.moreLength));
                return false;
            }
            return true;
        }
    }


}

