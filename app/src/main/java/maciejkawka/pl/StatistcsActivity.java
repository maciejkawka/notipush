package maciejkawka.pl;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import maciejkawka.pl.adapters.StatisticAdapterWithViewHolder;
import maciejkawka.pl.objects.AppStatistics;
import maciejkawka.pl.objects.SavedNotification;

public class StatistcsActivity extends AppCompatActivity {

    private TextView forNumberNotifications;
    private RecyclerView recyclerView;

    private List<SavedNotification> mListNotifications;
    private List<AppStatistics> statisticsLis;

    private StatisticAdapterWithViewHolder mAdapter;

    private RecyclerView.LayoutManager mlayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistcs);

        forNumberNotifications = findViewById(R.id.forNotiNumber);
        recyclerView = findViewById(R.id.forStatisticViewHolder);

        recyclerView.setHasFixedSize(false);
        mlayoutManager = new LinearLayoutManager(StatistcsActivity.this);
        recyclerView.setLayoutManager(mlayoutManager);

        if(mListNotifications == null) {
            mListNotifications = new ArrayList<>();
        }
        if(mListNotifications.size() > 0) {
            mListNotifications.clear();
        }
        loadSavedNotyfications();


        if(mListNotifications.size()>0){

            forNumberNotifications.setText(String.valueOf(mListNotifications.size()));

            if(statisticsLis == null)
            {
                statisticsLis = new ArrayList<>();
            }
            else
            {
                statisticsLis.clear();
            }

            for(int i = 0; i<mListNotifications.size(); i++)
            {
                SavedNotification tmpNoti = mListNotifications.get(i);
                boolean found = false;
                for(int a = 0; a< statisticsLis.size(); a++)
                {
                    AppStatistics tmpAppStatistic = statisticsLis.get(a);

                    if(tmpAppStatistic.getAppName().equals(tmpNoti.getAppName()))
                    {
                        tmpAppStatistic.increase();
                        found = true;
                        break;
                    }
                }
                if(!found)
                {
                    statisticsLis.add(new AppStatistics(tmpNoti.getAppName(), tmpNoti.getPackageName()));
                }

            }

            Collections.sort(statisticsLis);
            Collections.reverse(statisticsLis);

            mAdapter = new StatisticAdapterWithViewHolder(statisticsLis,mListNotifications.size(),this);
            recyclerView.setAdapter(mAdapter);

        }

    }


    private void loadSavedNotyfications()
    {
        if(mListNotifications == null)
            mListNotifications = new ArrayList<>();

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.spnotyfications),MODE_PRIVATE);

        Gson gson = new Gson();

        Map<String, ?> all = sharedPreferences.getAll();

        for(Map.Entry<String, ? > noty : all.entrySet())
        {
            String json = (String) noty.getValue();
            SavedNotification notyfication = gson.fromJson(json,SavedNotification.class);

            mListNotifications.add(notyfication);
        }


    }
}