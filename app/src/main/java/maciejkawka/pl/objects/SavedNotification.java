package maciejkawka.pl.objects;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;

import java.util.Date;

public class SavedNotification implements Comparable<SavedNotification> {


    public boolean equals(@Nullable SavedNotification obj) {
        if(this.getAppName().equals(obj.getAppName()) && this.getPackageName().equals(obj.getPackageName())
            && this.getText().equals(obj.getText()) && this.getTitle().equals(obj.getTitle()))
            return true;
        else
            return false;
    }

    @Override
    public int compareTo(SavedNotification o) {
        return this.date.compareTo(o.getDate());
    }

    @Expose
    private Date date;
    @Expose
    private String packageName;
    @Expose
    private String appName;
    @Expose
    private String title;
    @Expose
    private String text;


    public SavedNotification(Date date, String packageName, String appName, String title, String text) {
        this.date = date;
        this.packageName = packageName;
        this.appName = appName;
        this.title = title;
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
