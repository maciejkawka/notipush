package maciejkawka.pl.objects;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class GenerateID {

    static String id;

    public static String drawId(){

        id =  yearMonthandDay();
        id = id + drawRest();
        return id;
    }

    public static String drawRest() {
        Random generate = new Random();
        int rest = generate.nextInt(1000) +1;
        return String.valueOf(rest);
    }

    public static String yearMonthandDay() {
        String ID;
        DateFormat df = new SimpleDateFormat("yy");
        ID = df.format(Calendar.getInstance().getTime());
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int month = cal.get(Calendar.MONTH) + 1;

        switch (month){
            case 1:
                ID = ID + "A";
                break;
            case 2:
                ID = ID + "B";
                break;
            case 3:
                ID = ID + "C";
                break;
            case 4:
                ID = ID + "D";
                break;
            case 5:
                ID = ID + "F";
                break;
            case 6:
                ID = ID + "G";
                break;
            case 7:
                ID = ID + "H";
                break;
            case 8:
                ID = ID + "I";
                break;
            case 9:
                ID = ID + "J";
                break;
            case 10:
                ID = ID + "K";
                break;
            case 11:
                ID = ID + "L";
                break;
            case 12:
                ID = ID + "M";
                break;
            default:
                ID = ID + "X";
        }

        int day = cal.get(Calendar.DAY_OF_MONTH);

        if(day < 10)
            ID = ID + "0" + day;
        else
            ID = ID + day;
        return ID;
    }
}
