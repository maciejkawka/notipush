package maciejkawka.pl.objects;

public class AppStatistics implements Comparable<AppStatistics>{

    private int quantity;
    private String appName;
    private String packageName;



    public AppStatistics (String appName, String packageName) {
        this.quantity = 1;
        this.appName = appName;
        this.packageName = packageName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void increase(){
        this.quantity++;
    }


    @Override
    public int compareTo(AppStatistics o) {
        return Integer.compare(this.getQuantity(),o.getQuantity());

    }
}
