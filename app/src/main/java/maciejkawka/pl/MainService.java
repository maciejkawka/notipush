package maciejkawka.pl;


import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import maciejkawka.pl.objects.AppInfo;
import maciejkawka.pl.objects.SavedNotification;


public class MainService extends NotificationListenerService {

    String pack = "";
    String appname = "";
    String title = "";
    String text = "";
    Date czas;
    boolean knowWhereToSend;
    String myId;
    String listenningID;
    private DatabaseReference mDatabase;
    private DatabaseReference sender;
    private PackageManager packageManager;
    private List<AppInfo> apps;

    private SavedNotification tmpNotification;

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createNotificationChanel();
        createNotyficatioAndStartService();
        packageManager = getPackageManager();

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sfid),MODE_PRIVATE);
        myId = sharedPreferences.getString((String)getText(R.string.keyForid),"non");

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("ID_PHONES").child(myId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    listenningID = snapshot.getValue().toString();
                    if(listenningID.length() > 3)
                        knowWhereToSend = true;
                    else
                        knowWhereToSend = false;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        return START_STICKY;
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        if(!(sbn.getPackageName().equals("maciejkawka.pl") && (sbn.getId() == 666)))
        {
            apps = loadData();
            if (sbn.isClearable()) {
                if (apps != null) {
                    boolean exist = false;
                    for (int i = 0; i < apps.size(); i++) {
                        AppInfo tmpApp = apps.get(i);
                        String tmp = sbn.getPackageName();
                        if (tmp.equals(tmpApp.getPackageName()))
                            exist = true;

                        if (exist && tmpApp.getStatus())
                        {
                            getDataformNotyfication(sbn);
                            czas = new Date(sbn.getPostTime());
                            saveMyNotification(sbn.getPostTime());
                            if(knowWhereToSend)
                                sendMyNotyfication();

                            break;
                        }

                    }

                    if (!exist) {
                        showInformationNotification(getString(R.string.unApp),getString(R.string.doScan));
                    }
                } else {
                    showInformationNotification(getString(R.string.noScan),getString(R.string.doScan));
                }
            }
        }
    }

    private void sendMyNotyfication() {
        HashMap<String,String> message = new HashMap<>();
        message.put("title",title);
        message.put("appname",appname);
        message.put("text",text);
        String time = "";
        if(czas.getHours()<10){ time = "0"+czas.getHours();}
        else{ time = String.valueOf(czas.getHours());}
        if(czas.getMinutes()<10){ time += " : 0" + czas.getMinutes();}
        else{ time += " : " + czas.getMinutes() ;}
        if(czas.getSeconds()<10){ time += " : 0" + czas.getSeconds();}
        else{ time += " : " + czas.getSeconds() ;}


        message.put("time", time);

        sender = FirebaseDatabase.getInstance().getReference();
        sender.child("DB_ID_EX").child(listenningID).setValue(message);

    }


    @Override
    public void onDestroy() {
        stopForeground(true);
        stopSelf();
        // unbindService(MainSe);
        super.onDestroy();
    }

    private void showInformationNotification(String notiTitle, String notiText){
        Intent tapIntent = new Intent(this,SetingsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,1,tapIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"maciejkawka.pl");
        builder.setSmallIcon(R.drawable.ic_baseline_info_24);
        builder.setContentTitle(notiTitle);
        builder.setContentText(notiText);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(666,builder.build());
    }

    private void saveMyNotification(long postTime) {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.spnotyfications), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Date date = new Date(postTime);
        SavedNotification savedNotification = new SavedNotification(date, pack, appname, title, text);
        Gson gson = new Gson();

        if(tmpNotification == null)
        {
            String json = gson.toJson(savedNotification);
            String dataInLongString = Long.toString(postTime);
            editor.putString(dataInLongString, json);
            editor.commit();
            tmpNotification = savedNotification;
        }
        else
        {
            if(tmpNotification.equals(savedNotification))
            {
                //nie robię nic, bo po co drugi raz
            }
            else
            {
                String json = gson.toJson(savedNotification);
                String dataInLongString = Long.toString(postTime);
                editor.putString(dataInLongString, json);
                editor.commit();
                tmpNotification = savedNotification;
            }
        }


    }


    private void getDataformNotyfication(StatusBarNotification sbn) {
        pack = sbn.getPackageName();
        ApplicationInfo appInfo = null;

        try {
            appInfo = packageManager.getApplicationInfo(pack, 0);
            appname = (String) appInfo.loadLabel(packageManager);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Bundle extras = sbn.getNotification().extras;

        // tutaj przyps bo jak jest obrazek albo emoji to sb nie poradzi getCharSequence
        try {
            title = String.valueOf(extras.getString("android.title"));
        } catch (NullPointerException e) {
            e.printStackTrace();
            title = " ";
        }

        try {
            text = extras.getCharSequence("android.text").toString();
        } catch (NullPointerException e) {
            e.printStackTrace();
            text = " ";
        }


    }

    private void createNotyficatioAndStartService() {
        Intent intent1 = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent1, PendingIntent.FLAG_ONE_SHOT);

        Notification notification = new NotificationCompat.Builder(this, "maciejkawka.pl") //NotiPushId
                .setContentTitle("tytuł")
                .setContentText("Tekst Content")
                .setContentIntent(pendingIntent).build();

        startForeground(1, notification);


    }

    private void createNotificationChanel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    "maciejkawka.pl", "Foreground Service Noti Push", NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(notificationChannel);
        }
    }

    private List<AppInfo> loadData() {
        List<AppInfo> APKA;
        APKA = new ArrayList();

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sheredApps), MODE_PRIVATE);
        Gson gson = new Gson();

        Map<String, ?> all = sharedPreferences.getAll();
        for (Map.Entry<String, ?> entry : all.entrySet()) {
            String json = (String) entry.getValue();
            AppInfo apka = gson.fromJson(json, AppInfo.class);
            APKA.add(apka);
        }

        return APKA;
    }

}
