package maciejkawka.pl.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import maciejkawka.pl.R;
import maciejkawka.pl.ViewHolders.SetingsViewHolders;
import maciejkawka.pl.objects.AppInfo;

public class AppAdapterWithViewHolder extends RecyclerView.Adapter<SetingsViewHolders> {

    private List<AppInfo> apps;
    private Context context;
    private PackageManager packageManager;
    private OnAppSetingsClick mListener;

    public AppAdapterWithViewHolder(List<AppInfo> apps, Context context) {
        this.apps = apps;
        this.context = context;
        this.packageManager = context.getPackageManager();
    }

    public void setOnItemClickListener(OnAppSetingsClick mListener){
        this.mListener = mListener;
    }



    @NonNull
    @Override
    public SetingsViewHolders onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_item_layout,parent,false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        v.setLayoutParams(layoutParams);
        SetingsViewHolders svh = new SetingsViewHolders(v, mListener);
        return svh;
    }

    @Override
    public void onBindViewHolder(@NonNull SetingsViewHolders holder, int position) {

        holder.mTitle.setText(apps.get(position).getLabel());
        holder.mVerion.setText(apps.get(position).getVersion());
        holder.mSwitch.setChecked(apps.get(position).getStatus());
        holder.mSwitch.setTag(apps.get(position).getLabel());

        try{
            ApplicationInfo app = packageManager.getApplicationInfo(apps.get(position).getPackageName(),0);
            Drawable icon = app.loadIcon(packageManager);
            holder.imageView.setBackgroundDrawable(icon);
        } catch (PackageManager.NameNotFoundException e)
        {

            if((e.getStackTrace() != null))
                Log.e("DRAWABLE, AppAdapter" ,e.getMessage());

            if(e.getMessage() != null)
                Log.e("DRAWABLE, AppAdapter" ,e.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return apps.size();
    }
}
