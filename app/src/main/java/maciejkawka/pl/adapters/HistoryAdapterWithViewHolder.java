package maciejkawka.pl.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import maciejkawka.pl.R;
import maciejkawka.pl.ViewHolders.HistoryViewHolder;
import maciejkawka.pl.objects.SavedNotification;

public class HistoryAdapterWithViewHolder extends RecyclerView.Adapter<HistoryViewHolder>{

    private List<SavedNotification> mNotyfication;
    private Context context;
    private PackageManager packageManager;
    private OnHistoryClick mListener;

    public HistoryAdapterWithViewHolder(List<SavedNotification> mNotyfication, Context context, OnHistoryClick mListener)
    {
        this.mNotyfication = mNotyfication;
        this.context = context;
        packageManager = context.getPackageManager();
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item_layout,parent,false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        v.setLayoutParams(layoutParams);
        HistoryViewHolder statisticViewHolder = new HistoryViewHolder(v,mListener);
        return statisticViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {

        Date data = mNotyfication.get(position).getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        String datanotyfikacji;
        if(calendar.get(Calendar.DAY_OF_MONTH)<10)
            datanotyfikacji = "0" + calendar.get(Calendar.DAY_OF_MONTH)+".";
        else
            datanotyfikacji = calendar.get(Calendar.DAY_OF_MONTH)+".";
        int mon = calendar.get(Calendar.MONTH) +1;
        if(mon<10)
            datanotyfikacji += "0" + mon+".";
        else
            datanotyfikacji += mon+".";
        datanotyfikacji += String.valueOf(calendar.get(Calendar.YEAR));
        String godzinanotyfikacji;
        if(data.getHours()<10)
            godzinanotyfikacji = "0" +data.getHours()+":";
        else
            godzinanotyfikacji = data.getHours()+":";

        if(data.getMinutes()<10)
            godzinanotyfikacji += "0" +data.getMinutes();
        else
            godzinanotyfikacji += data.getMinutes();


        holder.mDate.setText(datanotyfikacji + " " + godzinanotyfikacji);
        holder.mTitle.setText(mNotyfication.get(position).getTitle());
        holder.mText.setText(mNotyfication.get(position).getText());
        holder.mAppName.setText(mNotyfication.get(position).getAppName());


        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(mNotyfication.get(position).getPackageName(),0);
            Drawable icon = applicationInfo.loadIcon(packageManager);
            holder.mImageView.setBackgroundDrawable(icon);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if(mNotyfication == null)
            return 0;
        else
            return mNotyfication.size();
    }
}
