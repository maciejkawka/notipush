package maciejkawka.pl.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import maciejkawka.pl.R;
import maciejkawka.pl.ViewHolders.StatisticsViewHolders;
import maciejkawka.pl.objects.AppStatistics;

public class StatisticAdapterWithViewHolder extends RecyclerView.Adapter<StatisticsViewHolders>  {

    private  List<AppStatistics> mList;
    private int quantityAll;
    private Context context;
    private PackageManager packageManager;

    public StatisticAdapterWithViewHolder(List<AppStatistics> mList, int quantityAll, Context context) {
        this.context = context;
        this.mList = mList;
        this.quantityAll = quantityAll;
        this.packageManager = context.getPackageManager();
    }

    @NonNull
    @Override
    public StatisticsViewHolders onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.statistic_item_layout,parent,false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        v.setLayoutParams(layoutParams);
        StatisticsViewHolders statisticsViewHolders = new StatisticsViewHolders(v);
        return statisticsViewHolders;
    }

    @Override
    public void onBindViewHolder(@NonNull StatisticsViewHolders holder, int position) {

        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(mList.get(position).getPackageName(),0);
            Drawable icon = applicationInfo.loadIcon(packageManager);
            holder.forImage.setBackgroundDrawable(icon);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        double licznik = mList.get(position).getQuantity();
        double naProcenty = licznik / quantityAll;
        naProcenty *= 10000;
        naProcenty = Math.round(naProcenty);
        naProcenty /= 100;
        holder.forPercent.setText(naProcenty + "%");
        holder.forAppName.setText(mList.get(position).getAppName());
        holder.forQuantity.setText(mList.get(position).getQuantity() + " :");
        naProcenty = naProcenty * 100;
        holder.forProgres.setProgress((int)naProcenty);


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
