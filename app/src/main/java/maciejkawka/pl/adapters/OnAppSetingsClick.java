package maciejkawka.pl.adapters;

public interface OnAppSetingsClick {
    void onItemClick(int position, boolean status);
}
