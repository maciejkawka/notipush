package maciejkawka.pl.adapters;

public interface OnHistoryClick {

    void onStatisticItemClick(int position);
}
