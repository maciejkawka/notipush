package maciejkawka.pl;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import maciejkawka.pl.adapters.AppAdapterWithViewHolder;
import maciejkawka.pl.adapters.OnAppSetingsClick;
import maciejkawka.pl.objects.AppInfo;

import static android.widget.Toast.makeText;

public class SetingsActivity extends AppCompatActivity {


    private Switch selectedApp;
    private Button buttonforscanApp;
    private Button buttonforClearHistory;
    private RecyclerView listAppView;

    private RecyclerView.LayoutManager mLayoutManager;
    private AppAdapterWithViewHolder mAdapter;

    private List<AppInfo> apps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setings);
        this.setTitle("Settings");

        selectedApp = findViewById(R.id.switSelectedApp);
        buttonforscanApp = findViewById(R.id.scanButton);
        buttonforClearHistory = findViewById(R.id.clearButton);

        listAppView = findViewById(R.id.appLIst);
        listAppView.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(SetingsActivity.this);

        listAppView.setLayoutManager(mLayoutManager);

        apps = loadData(apps);

        buttonforscanApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listAppView.setAdapter(null);
                apps.clear();
                LoadAppInfoTask loadAppInfoTask = new LoadAppInfoTask();
                loadAppInfoTask.execute(PackageManager.GET_META_DATA);
            }
        });

        buttonforClearHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAndClean();
            }


        });

        if(apps == null)
        {
            mAdapter = new AppAdapterWithViewHolder(null,SetingsActivity.this);
            apps = new ArrayList<>();
            buttonforscanApp.performClick();
        }
        else
        {
            Collections.sort(apps);
            mAdapter = new AppAdapterWithViewHolder(apps,SetingsActivity.this);
            listAppView.setAdapter(mAdapter);
        }


        mAdapter.setOnItemClickListener(new OnAppSetingsClick() {
            @Override
            public void onItemClick(int position, boolean status) {
                 apps.get(position).setStatus(status);
                 saveData(apps);
            }
        });
    }

    private void dialogAndClean() {

        AlertDialog alertDialog = new AlertDialog.Builder(SetingsActivity.this).create();
        alertDialog.setTitle(getString(R.string.askforDelete));
        alertDialog.setMessage(getString(R.string.warnmessage));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.remove),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.spnotyfications),MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.commit();
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.donotdelete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }


    private void saveData(List<AppInfo> APKA)
    {

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sheredApps),MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();

        for(int i = 0; i < APKA.size(); i++)
        {
            String json = gson.toJson(APKA.get(i));
            editor.putString(String.valueOf(i),json);
            editor.commit();
        }

    }

    private List<AppInfo> loadData(List<AppInfo> APKA)
    {
        if(APKA == null)
            APKA = new ArrayList();
        else
        {
            APKA.clear();
        }
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sheredApps),MODE_PRIVATE);
        Gson gson = new Gson();

        Map<String, ?> all = sharedPreferences.getAll();
        for(Map.Entry<String, ?> entry : all.entrySet())
        {
            String json = (String) entry.getValue();

            AppInfo apka = gson.fromJson(json, AppInfo.class);
            APKA.add(apka);
        }

        return APKA;

    }


    /**
     * Klasa do pobierania informacji o zainstalowanych apkach.
     */
    class LoadAppInfoTask extends AsyncTask<Integer,Integer, List<AppInfo>>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected List<AppInfo> doInBackground(Integer... integers)
        {
            //pozyskiwanie informacji o apkach
            apps = getInstaledAppList(integers[0]);
            return apps;
        }

        @Override
        protected void onPostExecute(List<AppInfo> appInfos)
        {
            super.onPostExecute(appInfos);
            Collections.sort(apps);
            mAdapter = new AppAdapterWithViewHolder(apps,SetingsActivity.this);
            listAppView.setAdapter(mAdapter);
            saveData(appInfos);
        }

        private List<AppInfo> getInstaledAppList(Integer... integers)
        {
            PackageManager packageManager = getPackageManager();

            List<ApplicationInfo> infos = packageManager.getInstalledApplications(integers[0]);
            ApplicationInfo selectedApp;
            PackageInfo packageInfo;

            for(int i = 0; i<infos.size(); i++)
            {
                selectedApp = infos.get(i);
                String label = (String)selectedApp.loadLabel(packageManager);
                String packaheInfo = (String) selectedApp.packageName;
                String version = "1";
                boolean flag;

                if((selectedApp.flags & ApplicationInfo.FLAG_SYSTEM) == 1 )
                     flag = false;
                else
                    flag = true;

                try {
                    packageInfo = packageManager.getPackageInfo(packaheInfo,0);
                    version = String.format("%s",packageInfo.versionName);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                apps.add(new AppInfo(label,version,packaheInfo,flag));
            }

            return apps;
        }

    }



}